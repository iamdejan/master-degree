# Universiti Sains Malaysia

## Degree

[MSc (Computer Science) - School of Computer Sciences](https://admission.usm.my/postgraduatev1/applied-sciences-research/msc-computer-science-school-of-computer-sciences-2).

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS:
  - Overall: 5
- TOEFL iBT:
  - Overall: 40
- Pearson Test of English (PTE) Academic:
  - Overall: 47

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Registration Fee | USD 222.5 |
| Personal Bond | USD 1000.0 |
| Tuition Fee | 2 years * USD 1550 / semester = USD 6200 |
| Thesis Fee | USD 250 |
| Graduation / convocation fee | USD 50 |
| Research methodology | USD 80 |

***Total***: USD 7802.5 -> IDR 123 mio
