# Xiamen University Malaysia

## Degree

[Master of Science in Mathematics and Applied Mathematics](https://www.xmu.edu.my/2020/0728/c16271a408366/page.htm).

## Duration

2 years.

## Available Research Topics

No specifics, unless applicants are applying for [XUM Research Fund](#xiamen-university-malaysia-research-fund-cycle-132024).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * MYR 13200 / year = MYR 26400 |
| Application Fee | MYR 100 |
| Registration Fee | MYR 100 |
| Hostel Application Fee | MYR 100 |
| International Student Fee | MYR 2500 |
| Security Deposit | MYR 1000 (refundable) |

***Total***: MYR 30200 -> IDR 110 mio

References: [here](https://www.xmu.edu.my/2022/0817/c16273a459387/page.htm)

## Scholarships

### Xiamen University Malaysia Research Fund Cycle 13/2024

Check [here](https://www.xmu.edu.my/18051/list.htm).
