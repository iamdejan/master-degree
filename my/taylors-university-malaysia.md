# Taylor's University

## Degree

[Master of Applied Computing](https://university.taylors.edu.my/en/study/explore-all-programmes/computer-science/master-coursework/master-of-applied-computing.html), with specialisation in "Artificial Intelligence".

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.0
- TOEFL iBT:
  - Overall: 60

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Approximate total fees | USD 13669 |

***Total***: USD 13669 -> IDR 225 mio

## Scholarships

### [High Achiever Scholarship 2024](https://university.taylors.edu.my/en/study/scholarships-and-financial-aid/postgraduate-scholarships.html#high-achiever-scholarship-2024)

Eligibility:
- General criteria:
  - Applicable to all students holding results from recognised institutions in Malaysia OR;
  - Applicable to Malaysian students holding results from recognised overseas institutions
  - Letter of Offer (LOO) required at point of application
  - Applicable for Full-time/Part-time students
  - Actual results required
- Academic criteria:
  - Bachelor’s Degree: Minimum CGPA (cummulative GPA): 3.50

The grant:
- 50% Tuition Fee Waiver

Assessments:
- Written assessment
- Interview
