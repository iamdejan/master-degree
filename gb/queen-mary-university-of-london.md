# Queen Mary University of London

## Degree

[Advanced Robotics MSc](https://www.qmul.ac.uk/postgraduate/taught/coursefinder/courses/advanced-robotics-msc/).

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Writing: 6
  - Reading: 5.5
  - Speaking: 5.5
  - listening: 5.5
- TOEFL iBT:
  - Overall: 92
  - Writing: 21
  - Reading: 18
  - Listening: 17
  - Speaking: 20

### Indonesian-specific Requirements

We normally consider the following qualifications for entry to our postgraduate taught programmes: Sarjna I (S1) Bachelor Degree or Diploma IV (D4) (minimum 4 years) from selected degree programmes and institutions.
- UK 1st class degree: GPA 3.6 to 3.8 out of 4.0
- UK 2:1 degree: GPA 3.0 to 3.2 out of 4.0
- UK 2:2 degree: GPA 2.67 to 2.8 out of 4.0

Offer conditions will vary depending on the institution you are applying from and the degree that you study.

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Conditional deposit | GBP 2000 |
| Tuition fee | GBP 28900 |

***Total***: GBP 30900 -> IDR 645 mio
