# University of Manchester

## Degree

[Advanced Computer Science](https://www.manchester.ac.uk/study/masters/courses/list/02069/msc-advanced-computer-science/).

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 7
  - Minimum per component: 6.5
- TOEFL iBT:
  - Overall: 100
  - Minimum per component: 22

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Deposit | GBP 1000 |
| Tuition Fee | 1 year * GBP 33000 / year = GBP 33000 |

***Total***: GBP 34000 -> IDR 655 mio
