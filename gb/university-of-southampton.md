# [University Name]

## Degree

[Artificial Intelligence (MSc)](https://www.southampton.ac.uk/courses/artificial-intelligence-masters-msc)

## Duration

1 year.

## Minimum Requirements

- Computer Science bachelor degree.

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 6.5
  - Reading: 6
  - Writing: 6
  - Speaking: 6
  - Listening: 6

### Indonesian-specific Requirements

Minimum cummulative GPA of 3 out of 4.

## Cost Calculation

Source: [here](https://www.southampton.ac.uk/courses/artificial-intelligence-masters-msc#funding).

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | GBP 32300 |
| Deposit | GBP 2000 |

***Total***: GBP 34300 -> IDR 680 mio
