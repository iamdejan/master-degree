# The University of Edinburgh

## Degree

[MSc Artificial Intelligence](https://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&edition=2024&id=107).

## Duration

1 year.

## Minimum Requirements

- Competence in programming is essential. During your degree you must have completed a programming course in at least one of the following: C/C++, Java, Python, R, Matlab, Haskell, ML.
- During your degree you must have completed the equivalent of 60 SCQF credits or 30 ECTS credits of mathematics that have typically covered the following subjects/topics: calculus (differentiation and integration), linear algebra (vectors and multi-dimensional matrices), discrete mathematics and mathematical reasoning (e.g. induction and reasoning, graph theoretic models, proofs), and probability (concepts in discrete and continuous probabilities, Markov chains etc.) Prior knowledge of probability concepts is especially important for this degree.

### English Language Proficiency

- IELTS Academic:
  - Overall: 7
  - Minimum per component: 6.5
- TOEFL iBT:
  - Overall: 100
  - Minimum per component: 23

### Indonesian-specific Requirements

Source: [here](https://www.ed.ac.uk/studying/international/postgraduate-entry/asia/indonesia).

- Minimum cummulative GPA is 3.2 out of 4.

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Deposit | GBP 1500 |
| Tuition Fee | GBP 40900 |

***Total***: GBP 42400 -> IDR 841 mio
