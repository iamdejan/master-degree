# University of Sheffield

## Degree

[MSc Robotics](https://www.sheffield.ac.uk/postgraduate/taught/courses/2024/robotics-msc)

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 6.5
  - Minimum per component: 6

### List of Required Documents

Source: [here](https://www.sheffield.ac.uk/postgraduate/supporting).

- Original bachelor degree's certificate, in native language.
- Translated bachelor degree's certificate.
- Original academic transcript, in native language.
- Translated academic transcript.
- Letter of reference.
  - Reference content: Your referee(s) should provide their personal opinion of your ability and suitability to undertake postgraduate study, in particular your previous academic achievements (especially in comparison to those of your peers), and any distinct strengths and weaknesses (for example motivation, commitment, independence, ability to work under sustained pressure).
  - How to provide a reference:
    - You will be able to send a request from the University of Sheffield to your referee(s) as part of the online application process. The request will be emailed automatically when you submit your application. You should normally use your referees' official university email addresses for references arranged in this way.
    - Alternatively, for most courses you can choose to upload references yourself to your online application. References should be uploaded as scanned copies of signed letters on official letterheaded paper.
- Supporting statement: You should supply a statement of at least 300 words giving your reasons for applying and any other information you want to include in support of your application. Among the issues you may wish to cover are how your previous education and experience relates to the course, and how the course fits into your long-term academic or career plans.
  - If needed, write a [research proposal](https://www.sheffield.ac.uk/academic-skills/study-skills-online/how-write-research-proposal).

### Indonesian-specific Requirements

- Bachelor's degree (S1) with cummulative GPA of 3.7 out of 4.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 1 year * GBP 29700 / year = GBP 29700 |

***Total***: GBP 29700 -> IDR 589 mio

## Scholarships

List of scholarships for international students can be found [here](https://www.sheffield.ac.uk/international/fees-and-funding/scholarships/postgraduate).
