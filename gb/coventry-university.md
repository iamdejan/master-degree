# Coventry University

## Degree

[Computer Science MSc](https://www.coventry.ac.uk/course-structure/pg/eec/computer-science-msc/)

## Duration

1 year.

## Minimum Requirements

For detailed list, contact student ambassador.

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 6.5
  - Minimum per component: 5.5
- [Coventry University English Language Test](https://www.coventry.ac.uk/international-students-hub/apply/english-tests/)
  - Overall: 5.5

If applicants didn't meet the standards, applicants are allowed to take [pre-sessional English (with additional fees)](https://www.coventry.ac.uk/international-students-hub/apply/pre-sessional-english/). The minimum requirement for pre-sessional English is IELTS UKVI overall 4.

### List of Required Documents

Minimum GPA: 2.6 out of 4.

Source: [Entry Requirements](https://www.coventry.ac.uk/international-students-hub/find-your-region/regional-entry-requirements/?region=South-East-Asia).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 1 year * GBP 25000 / year = GBP 25000 |

***Total***: GBP 25000 -> IDR 490 mio

## Scholarships

There's a discount of GBP 3000 for international students. Information can be seen [here](https://www.coventry.ac.uk/international-students-hub/apply-for-a-scholarship/#pathways).
