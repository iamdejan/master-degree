# University of Glasgow

## Degree

[Masters in Robotics & Artificial Intelligence](https://www.gla.ac.uk/postgraduate/taught/roboticsai/).

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6.0
- TOEFL iBT:
  - Overall: 79
  - Reading: 13
  - Listening: 12
  - Speaking: 18
  - Writing: 21
- Peason Test of English (PTE) Academic:
  - Overall: 59
  - Minimum per component: 59

### Indonesian-specific Requirements

Minimum GPA: 3.0 from [recognised institutions](https://www.gla.ac.uk/media/Media_830130_smxx.pdf).

See [here](https://www.gla.ac.uk/international/country/indonesia/) for complete and up-to-date requirements.

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Full tuition fee | GBP 31860 |
| Deposits | GBP 2000 |

***Total***: GBP 33860 -> IDR 702 mio

## Scholarships

### Southeast Asia Distinction Award

See [here](https://www.gla.ac.uk/scholarships/southeastasiadistinctionaward/).
