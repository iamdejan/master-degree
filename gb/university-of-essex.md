# University of Essex

## Degree

[MSc Intelligent Systems and Robotics](https://www.essex.ac.uk/courses/PG00627/1/MSc-Intelligent-Systems-and-Robotics).

## Duration

1 year.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic
  - Overall: 6.0
  - Minimum per component: 5.5

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Tuition fee | 1 year * GBP 22400 / year = GBP 22400 |

***Total***: GBP 22400 -> IDR 467 mio
