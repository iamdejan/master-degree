# University of Auckland

## Degree

[Master of Artificial Intelligence](https://www.auckland.ac.nz/en/study/study-options/find-a-study-option/master-of-artificial-intelligence--mai-.html)

or

[Master of Robotics and Automation Engineering](https://www.auckland.ac.nz/en/study/study-options/find-a-study-option/master-of-robotics-and-automation-engineering-mroboteng.html)

## Duration

At least a year.

## Minimum Requirements

GPE (Grade Point Equivalent): 4.0. See GPE calculator [here](http://www.gpecalculator.auckland.ac.nz/?utm_source=auckland&utm_medium=postgraduate).

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6.0
- TOEFL iBT:
  - Overall: 90
  - Writing: 21
- Pearson Test of English (PTE) Academic:
  - Overall: 58
  - Minimum per component: 50

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Total tuition fee | NZD 50810 |

***Total***: NZD 50180 -> IDR 503 mio
