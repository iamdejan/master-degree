# University of Otago

## Degree

[Master of Applied Science in Artificial Intelligence](https://www.otago.ac.nz/study/subjects/arfi).

## Duration

3 semesters (1.5 years).

## Minimum Requirements

Must be from Bachelor of Computer Science.

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.0
  - Minimum per component: 5.5
- TOEFL iBT:
  - Overall: 80
  - Writing: 20
- Pearson Test of English (PTE) Academic:
  - Overall: 50
  - Minimum per component: 42

### List of Required Documents

- Academic transcripts.
- Translation of documents.
  - For Binus University graduates, this is not necessary since all graduation documents are are bilingual.
- Educational Credential Evaluators report (ECE)
  - Not needed for Indonesians.
- Proof of your identity.
  - Passport or birth certificate, translated to English.
  - Must be certified by one of these authorities:
    - Your agent (if you applied for admission through an authorised agency)
    - A Lawyer
    - A Notary Public
    - A Justice of the Peace
    - A Commissioner of Oaths
    - A Court Registrar or Deputy Registrar
    - A Member of Staff at a New Zealand Embassy
    - A Member of the Exchange/Study Abroad Office at your home University
  - The authorised staff must note on the copied document:
    - "Certified true copy" (in English)
    - Their authority (in English)
    - Their name
    - Their signature
    - Their official stamp
    - Certification must be done in English for it to be accepted

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Tuition fee | 1.5 years * NZD 63624 / year = NZD 95436 |

***Total***: NZD 95436 -> IDR 960 mio

## Scholarships

### [University of Otago Coursework Master's Scholarship](https://www.otago.ac.nz/international/future-students/subjects-courses/postgraduate-study/scholarships/university-of-otago-coursework-masters-scholarship)

Due to the limited number of University of Otago Coursework Masters Scholarships available (20), application invitations are restricted by a nomination system. Once a student has been admitted to their programme of choice, the department will assess their suitability for scholarship funding and advise the Scholarships Office that they wish to nominate a candidate for this scholarship. The Scholarships Office will then send an invite to the candidate via eVision. Please note that due to the competitive nature of this scholarship, candidates who will likely be nominated for this scholarship will usually need to have a GPA of at least an A on their most recent year of study. A decision regarding these scholarships will be made by the month preceding commencement of the programme of study.
