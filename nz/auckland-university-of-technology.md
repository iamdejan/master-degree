# [Auckland University of Technology]

## Degree

[Master of Computer and Information Sciences](https://www.aut.ac.nz/study/study-options/engineering-computer-and-mathematical-sciences/courses/master-of-computer-and-information-sciences)

## Duration

1.5 years.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6.0

### Indonesian-specific Requirements

Minimum GPA: 3.00 out of 4.00.

See [here](https://www.aut.ac.nz/__data/assets/pdf_file/0009/904959/PG-Entry-Requirements-Indonesia-v3.pdf) for the complete list.

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Total fees | NZD 58775.75 |

***Total***: NZD 58775.75 -> IDR 590 mio
