# Leiden University

## Degree

[Computer Science](https://www.universiteitleiden.nl/en/education/study-programmes/master/computer-science).

Minor / specialisation option: [Advanced Computing and Systems](https://www.universiteitleiden.nl/en/education/study-programmes/master/computer-science/advanced-computing-and-systems).

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 90
  - Minimum per component: 20

### List of Required Documents

- Passport
- Indonesian ID card (KTP).
- CV / resume.
- English proficency test result(s).
- Bachelor degree's certificate.
- Academic transcript.
- Placement letter: a letter to state that the applicant has equal skills with computer science graduates from Leiden University.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * EUR 21200 / year = EUR 42400 |

***Total***: EUR 42400 -> IDR 714 mio

## Scholarships

List of scholarships can be seen [here](https://www.student.universiteitleiden.nl/en/scholarships?cd=guest&cf=leiden-university).
