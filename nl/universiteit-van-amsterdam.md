# [Universiteit Van Amsterdam]

## Degree

[Master Artificial Intelligence](https://www.uva.nl/shared-content/programmas/en/masters/artificial-intelligence/artificial-intelligence.html)

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

A Dutch or foreign qualification comparable to Artificial Intelligence or Computer Science, complemented by the following requirements:
- Basic knowledge of Computer Science (at least 12 EC)
- Basic programming skills (at least 12 EC)
- Basic university level Calculus (at least 6 EC)
- Basic university level Linear Algebra (at least 6 EC)
- Basic university level Probability and Statistics (at least 6 EC)
- A motivation which matches the content of the Master's programme.
- Proficiency requirement in English (see below)

Additional requirements
In addition to the admission requirements referred to above the student has to comply with the following additional requirements:

1. You have an overall grade point average (GPA) equivalent to at least:
  - 6.5 (according to the Dutch grading system)
  - 3.0 (American system)
  - 2.1 (a second class upper/division one degree in the British system)
  - C (ECTS-system)
2. The student has obtained the Bachelor's degree within at most 3 years more than the nominal duration of the programme.

A maximum of 200 students are admitted to the Master's programme Artificial Intelligence in 2024-2025.

Candidates will be selected in the following way. First, candidates will have to meet the requirements and additional requirements as stated above. Subsequently all the candidates eligible for admission to the programme will be ranked by the ranking criteria. Each ranking criterion will be classified/assessed according to five categories: excellent, very good, good, sufficient, insufficient.

Selection will be based on the following ranking criteria:
1. GPA score
2. Relevant AI projects (completed)
3. Programming skills
4. Knowledge of logic
5. Knowledge of mathematics
6. Topic of Bachelor thesis
7. Motivation for contents Master’s programme
8. Relevant AI courses at level of Master’s programme
9. Publications

The ranking criteria apply to all candidates who have met the requirements and additional requirements as stated above.

See [here](https://www.uva.nl/shared-content/programmas/en/masters/artificial-intelligence/application-and-admission/international-prior-education/international-prior-education-foldout-menu.html) for up-to-date information.

### English Language Proficiency

- IELTS Academic:
  - Overall: 7.0
  - Minimum per component: 6.5
- TOEFL iBT:
  - Overall: 100
  - Listening: 22
  - Reading: 24
  - Writing: 24
  - Speaking: 25

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Statutory tuition fee full-time | EUR 2530 |
| Institutional fee for non-EEA students | EUR 17380 |

***Total***: EUR 19910 -> IDR 351 mio
