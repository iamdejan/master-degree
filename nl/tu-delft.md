# [University Name]

## Degree

[MSc Computer & Embedded Systems Engineering](https://www.tudelft.nl/en/education/programmes/masters/msc-computer-embedded-systems-engineering)

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

Source: [here](https://www.tudelft.nl/en/education/admission-and-application/msc-international-diploma/admission-requirements).

- Cummulative GPA: 3 out of 4.
  - must be comparable in level to a bachelor’s degree from a research university in the Netherlands;
  - must be closely related to your MSc programme of choice;
  - scores for key subjects must also be good.

### English Language Proficiency

- IELTS Academic:
  - Overall: 7
  - Minimum per componnent: 6.5
- TOEFL iBT:
  - Overall: 100
  - Minimum per component: 22

### List of Required Documents

Source: [here](https://www.tudelft.nl/en/education/admission-and-application/msc-international-diploma/required-documents).

- Bachelor degree's certificate.
- Academic transcript.
- English proficiency test result(s).
- Motivation letter: A clear and relevant essay in English (1,000 – 1,500 words) addressing the following:
  - Your motivation for choosing this MSc programme.
  - Why you are interested in TU Delft and what you expect to find here.
  - If this MSc programme has specialisation(s), which specialisation interests you the most and why?
  - Describe your hypothetical thesis project: what kind of project would you prefer? What would you want to explore? Please limit your answer to three possible topics.
  - Summarize in a maximum of 250 words your BSc thesis work or final assignment/project. Please include information about the workload.
- Curriculum Vitae: An extensive curriculum vitae (resume) written in English.
  - The CV must at least contain the following information, listed from new to old:
    - Your personal information
    - Your work experience
    - Your education 
    - You may also list additional skills, interests, other things that you consider important for the selection committee to know.
  - As an example, you may use [Europass format](https://europa.eu/europass/en) to create your cv.
  - The CV can contain a maximum of 3 pages.
- Identity documents:
  - Passport.
  - Visa: stamped pages need to be photocopied.
- GRE General Test:
  - Minimum scores:
    - Verbal: 154
    - Quantitative: 163
    - Analytical: 4
  - Institute Code: 3823
  - Department: For 'school type' select 'graduate schools’. For 'department' select ‘any other department not listed'.

## Cost Calculation

Source:
- [Tuition Fee](https://www.tudelft.nl/en/education/practical-matters/tuition-fee-finances/)
- [Housing](https://www.tudelft.nl/en/education/practical-matters/housing)

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * EUR 21515 / year = EUR 43030 |
| Living costs (incl. housing, etc.) | 2 years * EUR 12450 / year = EUR 24900 |

***Total***: EUR 67930 -> IDR 1.2 bio

## Scholarships

List of scholarships can be found [here](https://www.tudelft.nl/en/education/practical-matters/scholarships).
