# Groningen University

## Degree

[Computing Science](https://www.rug.nl/masters/computing-science/)

## Minimum Requirements

2 years.

Minimum credit: 120 [ECTS](https://en.wikipedia.org/wiki/European_Credit_Transfer_and_Accumulation_System).

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6.5
- TOEFL iBT:
  - Overall: 90
  - Reading: 21
  - Listening: 21
  - Speaking: 21
  - Writing: 24

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * EUR 24200 / year = EUR 48400 |

***Total***: EUR 48400 -> IDR 815.1 mio
