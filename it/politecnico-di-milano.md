# Politecnico di Milano

## Degree

[Laura Magistrale (MSc) of Computer Science and Engineering](https://www.polimi.it/en/programmes/laurea-magistrale-equivalent-to-master-of-science/M/2023-ing-ind-inf-magord-270-mi-481-computer-science-and-engineering-ingegneria-informatica).

Additional information can be seen [here](https://yocket.com/universities/politecnico-di-milano/computer-science-and-engineering-74395).

## Duration

2 years.

## Minimum Requirements

Application procedures and minimum requirements can be seen [here](https://www.polimi.it/en/international-prospective-students/laurea-magistrale-programmes-equivalent-to-master-of-science/application-procedures).

### English Language Proficiency

- IELTS Academic
  - Overall: 6
- TOEFL iBT:
  - Overall: 78

### List of Required Documents

List of required documents:
- Bachelor degree's certificate.
- Academic transcript.
- Detail of GPA's calculation.
- CV
- English proficiency test result(s).
- GRE.
- Identity: passport, visa, and others.
- Letter of Recomendation: <b>A letter written by a current or former professor</b>. University Letterhead and/or an original stamp are welcome. Please note that there is no specific format required for the recommendation letter, but <b>it must contain the institutional contacts of the professor</b>. We advise you to ask a professor who knows you well to write it, to be personal and to show that he knows you are applying to Politecnico di Milano. <b>If you completed your studies a few years ago and it is not possible to get a letter from your former professor, a letter written by your manager or employer may be accepted</b>.
- Letter of Motivation: A letter of motivation (also called a "statement of purpose" or "personal essay") is a brief essay in which you explain what you hope to achieve, as well as what qualifies you as a top candidate for the admission, why you selected that specific programme, and why you have chosen Politecnico di Milano among other universities. If you apply to 2 courses, please write 2 different motivation letters (max. 2 pages each) and combine them into one pdf file to be uploaded on your profile.
- Syllabus of undergraduate courses: A document, 4 MB max, containing a brief but detailed description regarding the content of each course taken. If possible, it should contain the number of hours of the courses or training activities that you have completed according to your academic bachelor curriculum. It can be taken directly from the website of your university or it can be copied from the university course catalogue. If the course description is not available, you can write it yourself and, if possible, have it signed by the Head of the Department of your study programme.

Source: [Politecnico di Milano](https://www.polimi.it/en/international-prospective-students/laurea-magistrale-programmes-equivalent-to-master-of-science/application-procedures/application/list-of-documents-required-by-the-admissions-office)

### Indonesian-specific Requirements

Minimum GPA: 2.8 out of 4.

## Curriculum

Curriculum can be seen [here](https://www4.ceda.polimi.it/manifesti/manifesti/controller/ManifestoPublic.do?evn_default=EVENTO&aa=2023&k_cf=225&k_corso_la=481&ac_ins=0&k_indir=T2A&lang=EN&tipoCorso=ALL_TIPO_CORSO&semestre=ALL_SEMESTRI).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * EUR 3891.59 / year = EUR EUR 7783.18 |

***Total***: EUR 7783.18 -> IDR 133 mio
