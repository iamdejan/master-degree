# Politecnico di Torino

## Degree

- [ICT for Smart Societies](https://www.polito.it/en/education/master-s-degree-programmes/ict-for-smart-societies/programme-details)

## Duration

Not mentioned. Most likely 2 years.

## Minimum Requirements

Must be a Computer Science graduate.

### English Language Proficiency

- IELTS Academic
  - Overall: 5.5
- TOEFL iBT
  - Overall: 72
- Pearson Test of English (PTE) Academic
  - Overall: 59

### List of Required Documents

- Passport.
- Residence permit
- CV
- Bachelor degree's certificate.
- Academic transcript.
- Syllabus / description of undegraduate courses.
- English proficiency test result(s).
- Statement of Validity of Bachelor's degree or Statement of Comparability issued by the Italian ENIC-NARIC centre - CIMEA

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * EUR 3821 / years = EUR 7642 |

***Total***: EUR 7642 -> IDR 130 mio
