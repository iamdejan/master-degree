# ETH Zurich

## Degree

[Master Robotics, Systems and Control](https://ethz.ch/en/studies/master/degree-programmes/engineering-sciences/robotics-systems-and-control.html).

## Duration

1.5 years or 90 ECTS.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 7.0
  - Minimum per component: 6.0
- TOEFL iBT:
  - Overall: 100

Source: [here](https://ethz.ch/en/studies/master/application/language-requirements.html).

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Tuition fee | 3 * CHF 730 = CHF 2190 |

***Total***: CHF 2190 -> IDR 40 mio
