# The University of Sydney

## Degree

[Master of Computer Science](https://www.sydney.edu.au/content/courses/courses/pc/master-of-computer-science.html)

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 85
  - Reading: 17
  - Listening: 17
  - Speaking: 17
  - Writing: 19
- PTE Academic:
  - Overall: 61
  - Minimum per component: 54

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | AUD 150 |
| Tuition Fee | 2 years * AUD 53000 / year = AUD 106000 |

***Total***: AUD 106150 -> IDR 1.1 bio
