# The University of Melbourne

## Degree

[Master of Computer Science](https://study.unimelb.edu.au/find/courses/graduate/master-of-computer-science/)

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 79
  - Writing 21
  - Speaking 18
  - Reading 13
  - Listening 13
- Pearson Test of English (PTE) Academic:
  - Overall: 58
  - Minimum per component: 50

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Total tuition fee prediction | AUD 110274 |

***tOTAL***: AUD 110274 -> IDR 1.2 bio
