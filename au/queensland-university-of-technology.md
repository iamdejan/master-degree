# Queensland University of Technology

## Degree

[Master of Robotics and Artificial Intelligence](https://www.qut.edu.au/courses/master-of-robotics-and-artificial-intelligence?international)

## Duration

1.5 years.

## Minimum Requirements

- Bachelor degree must be STEM-related.

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 79
  - Listening: 16
  - Reading: 16
  - Writing: 21
  - Speaking: 18
- PTE Academic:
  - Overall: 58
  - Minimum per component: 50

### Indonesian-specific Requirements

- Applicants must pass `Academic English 5 (AE5) program` course.
  - This program is taken in the first year.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 1.5 years * AUD 41700 = AUD 62550 |

***Total***: AUD 62550 -> IDR 648.8 mio
