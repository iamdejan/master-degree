# University of New South Wales

## Degree

[Master of Information Technology](https://www.unsw.edu.au/study/postgraduate/master-of-information-technology?studentType=International)

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 90
  - Writing: 23
  - Reading: 22
  - Listening: 22
  - Speaking: 22
- Pearson Test of English (PTE) Academic:
  - Overall: 64
  - Minimum per component: 54

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Total tuition fee prediction | AUD 96785 |

***Total***: AUD 96785 -> IDR 1 bio

## Scholarships

Find list of scholarships [here](https://www.scholarships.unsw.edu.au/scholarships/search?faculty=E&study=020000&show=all).
