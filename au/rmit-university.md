# RMIT University

## Degree

[Master of Science (Computer Science)](https://www.rmit.edu.au/study-with-us/levels-of-study/research-programs/masters-by-research/master-of-science-computer-science-mr221)

## Duration

2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 79
  - Reading: 13
  - Listening: 12
  - Speaking: 18
  - Writing: 21
- Pearson Test of English (PTE) Academic:
  - Overall: 58
  - Minimum per component: 50

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * AUD 36480 / year = AUD 73920 |

***Total***: AUD 73920 = IDR 756 mio

## Scholarships

List of scholarships can be seen [here](https://www.rmit.edu.au/study-with-us/international-students/apply-to-rmit-international-students/fees-and-scholarships/scholarships).
