# McMaster University

## Degree

[Computing and Software (MEng)](https://www.eng.mcmaster.ca/cas/programs/degree-options/meng-computing-and-software/).

NOTE: Additional information can be seen [here](https://collegedunia.com/canada/university/40-mcmaster-university-hamilton/master-of-engineering-meng-computing-and-software-193146).

## Duration

2 years.

## Minimum Requirements

Two referrals from undergraduate professors are required. No documents are needed, but McMaster will contact the professors directly.

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
  - Minimum per component: 5.5
- TOEFL iBT:
  - Overall: 88
  - Writing: 550
  - Computer-based: 213

### List of Required Documents

Complete list of required documents can be found [here](https://www.eng.mcmaster.ca/cas/applicants/#tab-content-required-documents).

List of required documents:
- Academic transcript.
- Statement of interest
- Resume / CV
- English proficiency test result(s).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * CAD 23059 / year = CAD 46118 |

***Total***: CAD 46118 -> IDR 537 mio
