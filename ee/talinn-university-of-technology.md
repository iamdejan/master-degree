# Talinn University of Technology

## Degree

[e-Governance Technologies and Services](https://taltech.ee/en/masters-programmes/e-governance-technologies-and-services).

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

List of requirements and entrance test(s) can be seen [here](https://taltech.ee/en/apply).

### English Language Proficiency

[English language proficiency](https://taltech.ee/en/apply#p55):
- IELTS Academic:
  - Overall: 6
  - Minimum masing-masing komponen: 5.5
- TOEFL iBT:
  - Overall: 75
  - Reading: 18
  - Listening: 17
  - Speaking: 20
  - Writing: 17
- Pearson Test of English (PTE) Academic:
  - Overall: 55
  - Minimum per component: 55

### Indonesian-specific Requirements

- Minimum: bachelor degree (S1) is required
- Send these documents:
  - English proficiency test result(s).
  - Bachelor degree's certificate.
  - Academic transcript: undergraduate university should send the translated transcript directly to TalTech.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | EUR 100 |
| Tuition Fee | 2 years * EUR 6000 / year = EUR 12000 |

***Total***: EUR 12100 -> IDR 203 mio

## Scholarship

Source: [here](https://taltech.ee/en/student/scholarships).

### Specialty Scholarships

Source: [here](https://taltech.ee/en/admission/specialty-scholarships).

[Performance Scholarship](https://taltech.ee/en/performance-scholarship), with subsidy up to EUR 100 / month.
