# University of Tartu

## Degree

[Software Engineering](https://ut.ee/en/curriculum/software-engineering).

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

### English Language Proficiency

[English language proficiency](https://ut.ee/en/english-language-requirements):
- IELTS Academic:
  - Overall: 6
  - Minimum masing-masing komponen: 5.5
- TOEFL iBT:
  - Overall: 75
- Pearson Test of English (PTE) Academic:
  - Overall: 59

### Indonesian-specific Requirements

- Minimum: bachelor degree (S1) is required
- Send these documents:
  - English proficiency test result(s).
  - Bachelor degree's certificate.
  - Academic transcript: undergraduate university should send the translated transcript directly to TalTech.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | EUR 100 |
| Tuition Fee | 2 years * EUR 6000 / year = EUR 12000 |

***Total***: EUR 12100 -> IDR 203 mio

## Scholarships

See [Industrial Master's Programme for students](https://cs.ut.ee/en/content/industrial-masters-programme-for-students). Apply at 1st year.
