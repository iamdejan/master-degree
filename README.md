# Master Degree

This repository is a collection of master degrees at universities outside of Indonesia.

Each university can be seen at each country. Each country has a folder, with naming convention from [ISO 3166 alpha-2 standard](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements).

## Australia

- [x] [RMIT University](./au/rmit-university.md)
- [x] [The University of Sydney](./au/the-university-of-sydney.md)
- [x] [The University of Melbourne](./au/the-university-of-melbourne.md)
- [x] [University of New South Wales](./au/university-of-new-south-wales.md)
- [x] [Queensland University of Technology](./au/queensland-university-of-technology.md)

## Canada

- [x] [McMaster University](./ca/mcmaster-university.md)

## Denmark

- [x] [Aarhus University](./dk/aarhus-university.md)

## Estonia

- [x] [Talinn University of Technology](./ee/talinn-university-of-technology.md)
- [x] [University of Tartu](./ee/university-of-tartu.md)

## Italy

- [x] [Politecnico di Milano](./it/politecnico-di-milano.md)
- [x] [Politecnico di Torino](./it/politecnico-di-torino.md)

## Malaysia

- [x] [Xiamen University Malaysia](./my/xiamen-university-malaysia.md)
- [x] [Universiti Sains Malaysia](./my/universiti-sains-malaysia.md)
- [x] [Taylor's University](./my/taylors-university-malaysia.md)

## Netherlands

- [x] [Leiden University](./nl/leiden-university.md)
- [x] [Groningen University](./nl/groningen-university.md)
- [x] [TU Delft](./nl/tu-delft.md)
- [x] [Universiteit Van Amsterdam](./nl/universiteit-van-amsterdam.md)

## New Zealand

- [x] [University of Otago](./nz/university-of-otago.md)
- [x] [Auckland Unviversity of Technology](./nz/auckland-university-of-technology.md)
- [x] [University of Auckland](./nz/university-of-auckland.md)

## Sweden

- [x] [Lund University](./se/lund-university.md)
- [x] [Umeå University](./se/umea-university.md)
- [x] [KTH Royal Institute of Technology](./se/kth-royal-institute-of-technology.md)

## Switzerland

- [x] [ETH Zurich](./ch/eth-zurich.md)

## United Kingdom of Great Britain and Northern Ireland

- [x] [University of Manchester](./gb/university-of-manchester.md)
- [x] [Coventry University](./gb/coventry-university.md)
- [x] [University of Southampton](./gb/university-of-southampton.md)
- [x] [University of Sheffield](./gb/uninversity-of-sheffield.md)
- [x] [The University of Edinburgh](./gb/the-university-of-edinburgh.md)
- [x] [University of Essex](./gb/university-of-essex.md)
- [x] [Queen Mary University of London](./gb/queen-mary-university-of-london.md)
- [x] [University of Glasgow](./gb/university-of-glasgow.md)

Additional reference regarding UKVI and SELT: [SELT English Exams for UKVI: A Guide for Learners](https://albaenglish.co.uk/blog/selt-ukvi-english-test-exam/)

## United States

- [x] [University of Coloardo, Boulder](./us/university-of-colorado-boulder.md)
- [x] [Colorado School of Mines](./us/colorado-school-of-mines.md)
- [x] [University of North Carolina, Chapel Hill](./us/unc-chapel-hill.md)
- [x] [University of Texas, Austin](./us/university-of-texas-austin.md)
- [x] [University of North Carolina, Charlotte](./us/unc-charlotte.md)
- [x] [University of Illinois, Urbana-Champaign](./us/university-of-illinois-urbana-champaign.md)
- [x] [Northwestern University](./us/northwestern-university.md)
- [x] [Georgia Institute of Technology](./us/georgia-institute-of-technology.md)
- [x] [Rice University](./us/rice-university.md)

Additional reference regarding credit hour: [How many credits do you need for a Bachelor’s or Master’s degree in the US?](https://www.mastersportal.com/articles/1110/what-you-need-to-know-about-the-american-university-credit-system.html)
