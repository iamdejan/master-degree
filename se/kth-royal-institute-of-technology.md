# [University Name]

## Degree

[MSc Systems, Control and Robotics](https://www.kth.se/en/studies/master/systems-control-robotics)

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

Academic qualifications from an internationally recognised university equivalent to a Swedish bachelor's degree.

A Bachelor's degree, corresponding to 180 ECTS, in computer science, electrical engineering, mathematics, physics, vehicle engineering, mechanical engineering or similar. Previous education must include basic mathematics courses in linear algebra, analysis in one and several variables, probability theory, programming and computer science. The student must also have knowledge of differential equations and control technology.

Within the programme there may be courses with additional requirements. The requirements are stated under the heading "Special eligibility" in the syllabus.

See [here](https://www.kth.se/en/studies/master/systems-control-robotics/entry-requirements-for-systems-control-and-robotics-1.8734).

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
- TOEFL iBT:
  - Overall: 90

### List of Required Documents

List of general documents can be seen [here](https://www.kth.se/en/studies/master/systems-control-robotics/entry-requirements-for-systems-control-and-robotics-1.8734).

List of specific documents for Systems, Control and Robotics:
- CV: KTH does not require a standard template, but the CV should clearly and concisely outline your education, work experience, computer literacy, qualifications and achievements (awards, publications, expertise knowledge and skills, etc.). It must be written in English.
- Letter of motivation: The letter of motivation explains why you have chosen this programme at KTH, what you hope to gain from it and how your interests and skills will help you succeed in your studies. Include an autobiography with the development and relevance of your academic and professional pursuits, extra-curricular activities and related experiences. KTH does not require a standard template, but it must be in English and less than 500 words. If you apply to multiple programmes that require a letter of motivation, you should submit one for each and state which programme each letter applies to at the top of the page.
- Letters of recommendation: Two letters of recommendation describing why you are the right choice for admission. Preferably, have one referee from an academic environment and one from a professional setting. If you apply for multiple programmes at KTH that request letters of recommendation, you may use the same letters for several programmes. You must provide an English translation if the letters are in a different language. The letters must have the referees' full contact details. If your referee wants to submit the letter themselves, they must send it by post to [University Admissions](https://bit.ly/2NQFQuV) and state your name and application number. They can not submit the letter via email.
- Summary sheet: The selection process is based on the following criteria: study results (grades), motivation for the studies (letter of motivation, references, courses and/or relevant work experience) and/or previous education. Previous education is assessed on the quality that the previous education is considered to have in the subjects that are relevant to the education applied for. To assess this, e.g. UKÄ's quality review for Swedish higher education institutions. For foreign universities, e.g. university ranking. The merit evaluation is done on a scale of 1-75.

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Application fee | SEK 900 |
| Total tuition fees | SEK 342000 |

***Total***: SEK 342900 -> IDR 537 mio
