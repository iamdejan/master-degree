# Lund University

## Degree

[Machine Learning, Systems and Control - Master's Programme](https://www.lunduniversity.lu.se/lubas/i-uoh-lu-TAMSR).

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

### English Language Proficiency

- IELTS Academic UKVI:
  - Overall: 7
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 100
  - Writing: 20 (dari skala 0-30)
- Pearson Test for English (PTE) Academic:
  - Overall: 68
  - Writing: 61

Source: [English Language Requirements](https://www.universityadmissions.se/en/entry-requirements/english-language-requirements/)

### Indonesian-specific Requirements

Complete list can be seen [here](https://www.universityadmissions.se/en/apply-to-masters/provide-application-documents-masters/country-instructions/indonesia/).

Required documents:
- Bachelor degree's certificate.
- Academic transcript.
- English proficiency test result(s).
- [Summary sheet](https://www.lunduniversity.lu.se/sites/www.lunduniversity.lu.se/files/2023-10/Summary-sheet-machine-learning.pdf).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | SEK 900 |
| First Fee | SEK 85000 |
| Tuition Fee | SEK 340000 / year = SEK 340000 |

***Total***: SEK 425900 -> IDR 670 mio
