# Umeå University

## Degree

[Master's Programme in Robotics and Control](https://www.umu.se/en/education/master/masters-programme-in-robotics-and-control/).

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

- Have a Bachelor's degree from an internationally recognised university.

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
- TOEFL iBT:
  - Overall: 90

### Indonesian-specific Requirements

Need to be graduated from Bachelor (S1), with minimum credits of 144.

See [here](https://www.universityadmissions.se/en/apply-to-masters/provide-application-documents-masters/country-instructions/indonesia/).

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Total fees | SEK 285600 |

***Total***: SEK 285600 -> IDR 447 mio
