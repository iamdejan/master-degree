# Georgia Institute of Technology

## Degree

- [Computer Science](https://grad.gatech.edu/degree-programs/computer-science-campus)
- [Robotics](https://grad.gatech.edu/degree-programs/robotics)

## Duration

Not mentioned. Assumed 1 year.

## Minimum Requirements

- Minimum GPA: 3 out of 4.
- Letters of Recommendation: 3 statements from ex-professors / ex-colleagues.
- Statement of Purpose.
- GRE General Test for Computer Science:
  - Minimum scores:
    - Verbal: 153
    - Quantitative: 155
    - Analytical: 3.0
  - Institute Code: R5248
  - Department Code: 0402
- GRE General Test for Robotics: optional (but recommended) and no minimum score.

### English Language Proficiency

- IELTS Academic:
  - Overall: 7.5
  - Reading: 6.5
  - Listening: 6.5
  - Speaking: 6.5
  - Writing: 5.5
- TOEFL iBT:
  - Overall: 100
  - Minimum per component: 19

## Cost Calculation

Assumption: 1 year (2 semesters).

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 1 year * USD 14570 / semester = USD 29140 |
| Mandatory Fee | USD 753 |


***Total***: USD 29893 -> IDR 465 mio
