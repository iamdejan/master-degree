# University of North Carolina, Charlotte

## Degree

[Computer Science](https://cci.charlotte.edu/academics/computer-science/masters-programs/)

## Duration

Not mentioned.

## Minimum Requirements

- Academic transcript.
- Bachelor degree's certificate.
- 3 reference letters.
- GRE General Test: no minimum score, but applicants still have to submit.
- Statement of Purpose.

### English Language Proficiency

- TOEFL iBT:
  - Overall: 83
  - [Must be sent directly from ETS portal](https://global-exam.com/blog/en/steps-to-send-toefl-scores-to-us-universities/). School code: 5105
- IELTS:
  - Overall: 6.5
  - Test result should be officially sent directly from test sites to UNC Charlotte.
- Duolingo English Test:
  - Overall: 115
  - Test results should be electronically send to UNC Charlotte through Duolingo's portal.

Source: [English Language Proficiency](https://gradadmissions.charlotte.edu/admissions/international-applicants/english-language-proficiency)

## Cost Calculation

Need credit hour calculation.
