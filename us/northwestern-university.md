# Northwestern Universities

## Degree

[Master of Science in Robotics](https://www.mccormick.northwestern.edu/robotics/)

## Duration

12-15 months.

## Minimum Requirements

- Bachelor degree must be STEM-related.
- Minimum GPA: 3 out of 4.
- Academic transcript have to be scanned and sent in admission process.
  - If candidates are accepted, then undergraduate university has to send directly the academic transcript to Northwestern.
- Letters of Recommendation: 3 persons (undergraduate professors or ex-managers) who can guarantee the quality of the applicants.
- GRE General Test: optional, but recommended.
- Resume.
- Personal Statement: applicants have to explain why applicants are interested in robotics.

### English Language Proficiency

- IELTS:
  - Overall: 7
- TOEFL iBT:
  - Overall: 100

## Cost Calculation

***Total***: USD 120000 -> IDR 1.1 bio
