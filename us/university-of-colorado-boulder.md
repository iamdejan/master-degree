# University of Colorado, Boulder

## Degree

- [Computer Science](https://www.colorado.edu/academics/grad-computer-science)
- [Robotics](https://www.colorado.edu/program/robotics/academics/master-science-professional)

## Duration

Not mentioned. Estimation is between 1-2 years.

## Minimum Requirements

### English Language Proficiency

- IELTS:
  - Overall: 7
- Duolingo English Test:
  - Overall: 120

Source: [English Proficiency Requirements](https://www.colorado.edu/graduateschool/admissions/where-begin/international-students/english-proficiency-requirements)

### Indonesian-specific Requirements

Minimum score is 7 from scale of 0-10. This reference is not clear, but if this is about GPA, then minimum GPA is 3 out of 4.

Source: [Country-Specific Requirements](https://www.colorado.edu/graduateschool/admissions/prepare-apply/international-students/country-specific-requirements)

## Cost Calculation

University fees (tuition, etc.) can be seen [here](https://www.colorado.edu/bursar/sites/default/files/attached-files/2023-24gradoutintrev100423.pdf).
Additional fees can be seen [here](https://www.colorado.edu/bursar/costs/mandatory-student-fees).

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 30 credit hour * USD 18810 / 10 credit = USD 56430 |
| Books and supplies | 30 credit hour * USD 75 / credit hour = USD 2250 |
| New Student Fee | USD 145 |
| Immigration Compliance Fee | 2 years (assumption) * USD 40 / semester = USD 160 |
| Arts & Cultural Enrichment Fee | 2 years (assumption) * USD 10 / semester = USD 40 |
| Bike Program Fee | 2 years (assumption) * USD 15 / semester = USD 60 |
| Career Services Fee | 2 years (assumption) * USD 12 / semester = USD 48 |
| Mental Health Resource Fee | 2 years (assumption) * USD 100 / semester = USD 400 |
| Recreation Center Expansion Fee | 2 years (assumption) * USD 110 / semester = USD 440 |
| Student Activity Fee | 2 years (assumption) * USD 310 / semester = USD 1240 |
| Student Computing Fee | 2 years (assumption) * USD 70 / semester = USD 280 |
| Student Health Fee | 2 years (assumption) * USD 100 / semester = USD 400 |
| Transit Pass Fee | 2 years (assumption) * USD 90 / semester = USD 360 |

***Total***: USD 62253 -> IDR 977 mio
