# University of Texas, Austin

## Degree

- [Computer Science (Coursework)](https://www.cs.utexas.edu/graduate-program/masters-program/coursework-option) with [additional robotics certification](https://robotics.utexas.edu/graduate)
- [Electrical and Computer Engineering](https://www.ece.utexas.edu/academics/graduate/tracks/dice) with minor in [Decision, Information, and Communications Engineering (DICE)](https://www.ece.utexas.edu/academics/graduate/tracks/dice)

## Duration

Not mentioned. Assumed 2 years.

## Minimum Requirements

- GRE General Test: optional, but recommended.
- Statement of Purpose
- CV / resume
- Letters of recommendation.

Source: [Admissions](https://www.ece.utexas.edu/academics/graduate/admissions)

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
- TOEFL iBT:
  - Overall: 79

Source: [Admissions](https://www.ece.utexas.edu/academics/graduate/admissions)

## Cost Calculation

Fees (including living costs, accomodation, and transportation) are estimated at USD 24572 / year.

***Total***: USD 48144 -> IDR 761 mio

Source: [Cost & Tuition Rates](https://onestop.utexas.edu/managing-costs/cost-tuition-rates/).
