# University of North Carolina, Chapel Hill

## Degree

[Computer Science](https://gradschool.unc.edu/programs/#494)

## Duration

Not mentioned. Assumed 2 years.

## Minimum Requirements

- GRE General Test are optional, but recommended.
- Statement of purpose: statement in 1-2 pages that contain:
  - The purpose of the study.
  - Interest in computer science.
  - Relevant and interesting informations about applicants that are not included in other documents, e.g. undergraduate projects, scientific papers, or experiences outside of academia.
  - List of courses that want to be taken by applicants, but do not exist in academic transcript.
- CV / resume: similar to job application, but academic oriented.
- Faculty Interests: *list down* 5 faculty members that the applicants want to collaborate with.

Source: [Graduate Admissions](https://cs.unc.edu/graduate/graduate-admissions/)

### English Language Proficiency

Applicants should include either:
- IELTS Academic; or
- TOEFL iBT.

But, no minimum score.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Tuition Fee | 2 years * USD 14422 / semester = USD 57688 |
| Mandatory health insurance | 2 years * USD 1392.4 / semester = USD 5569.6 |

***Total***: USD 63257.6 -> IDR 1 bio
