# Rice University

## Degree

[Master of Computer Science](https://csweb.rice.edu/academics/graduate-programs/professional-masters-programs).

## Duration

Not mentioned. Assumed 1 year.

Minimum: 30 study hours.

## Minimum Requirements

### English Language Proficiency

- IELTS:
  - Overall: 7
- TOEFL iBT:
  - Overall: 90

### List of Required Documents

Source: [here](https://csweb.rice.edu/academics/graduate-programs/admission/graduate-program-application)

- Letters of Recommendation: At least three letters of recommendation are required. The online application system will require you to submit letter writer names and email addresses. The system will then send to each of your letter writers an email message that includes a secure link to the online system, which will allow your letter writers to submit letters on your behalf. Visit the Office of Graduate and Postdoctoral Studies [Letters of Recommendation](https://graduate.rice.edu/lor) page for additional information.
- Official Academic Transcripts: Electronic copies of transcripts are required from each college or university that you have attended; use the online application system to submit all electronic transcripts. If you are offered admission to Rice, you will be required to have official copies of each transcript mailed directly to the department.
- GRE: Applicants may optionally (but are not required to) submit scores for the Graduate Record Examinations (GRE) general exam. The ETS school code for Rice is 6609. GRE subject test scores are not required.
- English proficiency test result(s).
  - TOEFL school code: `6609`.
- Statement of Purpose
- CV/Resume

## Available Research Topics

- Artificial Intelligence.
- Bioinformatics/Computational Biology.
- Data Science and Machine Learning.
- Management and Leadership.
- Systems and Security.

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | USD 85 |
| Tuition Fee | 1 year * USD 54630 / year = USD 54630 |

***Total***: USD 54630 -> IDR 853 mio
