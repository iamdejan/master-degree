# University of Illinois, Urbana-Champaign

Abbreviated as UIUC.

## Degree

[Master of Science in Computer Science](https://cs.illinois.edu/academics/graduate/ms-program)

## Duration

Not mentioned.

## Minimum Requirements

Complete requirements can be seen [here](https://cs.illinois.edu/admissions/graduate/applications-process-requirements).

### English Language Proficiency

- IELTS Academic:
  - Overall: 103
- TOEFL iBT:
  - Overall: 7.5

### Indonesian-specific Requirements

- Documents:
  - Bachelor degree's certificates.
    - Need to be translated to English by undergraduate university.
  - Academic transcript.
    - Need to be translated to English by undergraduate university.
  - English proficiency test result.
  - Documents are send to envelop that belongs to undergraduate university, sealed, and sent directly to UIUC.
- Minimum GPA: 3 out of 4.

Source: [here](https://grad.illinois.edu/admissions/4029/indonesia)

## Cost Calculation

No information regarding tuition fees.
