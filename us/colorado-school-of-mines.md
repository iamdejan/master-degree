# Colorado School of Mines

## Degree

- [Computer Science](https://gradprograms.mines.edu/computer-science-graduate-program/).
- [Robotics](https://gradprograms.mines.edu/robotics-graduate-program/).

## Duration

Not written. Assumed 1 year.

## Minimum Requirements

- GRE General Test: Graduate Record Examination (Quantitative section) score of 151 or higher (or 650 on the old scale). Applicants who have graduated with a computer science, engineering, or math degree from Mines within the past five years are not required to submit GRE General Test scores.
- Letters of Recommendation: Required – three letters.
- Resume or Curriculum Vitae (CV): Required
- Statement of Purpose: Required
- Transcript(s): Required. Must be submitted for all schools attended (unofficial transcripts accepted for admissions review and must show successful completion of any required prerequisite course(s)).

### English Language Proficiency

- IELTS:
  - Overall: 6.5
  - Minimum per component: 6
- TOEFL iBT:
  - Overall: 79
  - Reading: 20
  - Listening: 21
  - Writing: 17
  - Speaking: 21
- Duolingo English Test:
  - Overall: 105
  - Minimum per component: 100

Source: [Graduate Admissions - Program Requirements](https://www.mines.edu/graduate-admissions/admissions-requirements/)

## Cost Calculation

Source: [here](https://finaid.mines.edu/graduate-student-cost-of-attendance/).

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | USD 95 |
| Tuition Fee | 1 year * USD 41850 / year = USD 41850 |
| Housing Fee | 1 year * USD 17361 / year = USD 17361 |
| Books and supplies | 1 year * USD 1500 / year = USD 1500 |
| Daily costs | 1 year * USD 1611 / year = USD 1611 |
| Transportation costs | 1 year * USD 1665 / year = USD 1665 |

***Total***: USD 66613 -> IDR 1.1 bio (including living costs)
