# [University Name]

## Degree

[Degree name]

## Duration

<!-- Write minimum duration here. Of, if not applicable, write minimum number of credits need to be taken. -->

## Minimum Requirements

<!-- List down minimum requirements here. -->

### English Language Proficiency

### List of Required Documents

<!-- List down required documents. -->

### Indonesian-specific Requirements

## Available Research Topics

## Notable Lecturers

## Cost Calculation

NOTE: Currency uses [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217#Active_codes_(list_one)) standard.

| Fee Name | Fee (original currency) |
|---|---|
| Fee #1 | fee |
| Fee #1 | fee |

***Total***: [fee in original currency] -> [converted fee to Rupiah]

## Scholarships

### [Scholarship 1]

Requirements:
- [Scholarship requirement #1]
- [Scholarship requirement #2]
- [Scholarship requirement #3]
- ...
- [Scholarship requirement #n]
