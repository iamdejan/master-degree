# Aarhus University

## Degree

[Computer Science](https://masters.au.dk/computerscience).

## Duration

2 years.

Minimum credit: 120 ECTS.

## Minimum Requirements

List of requirements can be seen [here](https://masters.au.dk/am-i-qualified-for-admission).

For this degree, there are mandatory subjects that need to be taken at undergraduate level:
- Computational theory:
  - Formal language.
  - Compilation technique.
  - Math logic and proof.
  - Algorithm analysis and design.
  - Data structure.
- Programming:
  - OOP
  - Functional.
  - Software architecture.
  - Interaction design.
- Computer systems:
  - Computer (CPU) architecture.
  - Operating system.
  - Distributed systems.
  - Cybersecurity.
  - Database.

For students outside of Denmark, students need to submit [appendix document](https://masters.au.dk/fileadmin/www.masters.au.dk/Admission_filer/Appendix_to_application_NatTech_MSc_programmes_ver2_-_CS.pdf).

### English Language Proficiency

- IELTS Academic:
  - Overall: 6.5
- TOEFL iBT:
  - Overall: 83

### List of Required Documents

Checklist can be seen [here](https://masters.au.dk/admission/application-procedure/what-do-you-need-to-upload-along-with-your-application).

List of required documents:
- Application fee invoice.
- Bachelor degree's certificate.
- Undergraduate academic transcript, with translation to English.
- Official annotation / description regarding the grading system from undergraduate university.
- Description for each course taken in undergraduate level.
  - Applicants are allowed to translate the courses to English language if the source is given.
- English proficienty test result(s).

### Indonesian-specific Requirements

See list of the requirements [here](https://international.au.dk/education/meetau/country/indonesia).

## Cost Calculation

| Fee Name | Fee (original currency) |
|---|---|
| Application Fee | DKK 750 or EUR 100 |
| Tuition Fee | 2 years * EUR 15300 / year = EUR 30600 |

***Total***: EUR 30700 -> IDR 515 mio
